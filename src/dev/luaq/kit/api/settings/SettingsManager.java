package dev.luaq.kit.api.settings;

import dev.luaq.kit.KitCore;
import lombok.Getter;
import lombok.SneakyThrows;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class SettingsManager {
    @Getter private final List<PlayerSettings> playerCache = new ArrayList<>();
    @Getter private final String saveFile;

    @Getter private YamlConfiguration config;

    public SettingsManager(String saveFile) {
        this.saveFile = saveFile;

        // create the config file
        this.config = new YamlConfiguration();
    }

    public void removeFromCache(Player player) {
        Iterator<PlayerSettings> iterator = playerCache.iterator();
        while (iterator.hasNext()) {
            PlayerSettings settings = iterator.next();
            if (!settings.getPlayerId().equals(player.getUniqueId())) {
                continue;
            }

            // remove the player from the cache
            iterator.remove();
            break;
        }
    }

    public PlayerSettings settingsFor(Player player) {
        // check the cache for settings
        for (PlayerSettings settings : playerCache) {
            if (!settings.getPlayerId().equals(player.getUniqueId())) {
                continue; // not the player you are looking for
            }
            return settings; // found the proper settings file
        }

        PlayerSettings settings = new PlayerSettings(this, player);
        playerCache.add(settings); // add the new settings to the cache

        return settings;
    }

    public void loadSettings() {
        File saveFilePath = getSaveFilePath();
        if (!saveFilePath.exists()) {
            // save the file
            saveSettings();
            return;
        }

        // load the config from the file
        config = YamlConfiguration.loadConfiguration(saveFilePath);
    }

    @SneakyThrows
    public void saveSettings() {
        // save the file to the save path
        config.save(getSaveFilePath());
    }

    public List<PlayerSettings> allPlayerSettings() {
        return config.getKeys(false).stream()
                .map(UUID::fromString).map(Bukkit::getOfflinePlayer)
                .map(offlinePlayer -> new PlayerSettings(this, offlinePlayer))
                .collect(Collectors.toList());
    }

    public File getSaveFilePath() {
        return new File(KitCore.instance.getDataFolder(), saveFile);
    }
}
