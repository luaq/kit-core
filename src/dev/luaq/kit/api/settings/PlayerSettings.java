package dev.luaq.kit.api.settings;

import lombok.Getter;
import org.bukkit.OfflinePlayer;

import java.util.UUID;
import java.util.function.Function;

public class PlayerSettings {
    @Getter private final SettingsManager manager;
    @Getter private final UUID playerId;

    PlayerSettings(SettingsManager manager, OfflinePlayer player) {
        this.manager = manager;
        this.playerId = player.getUniqueId();

        // keep an updated IGN
        setSetting("name", player.getName());
    }

    public <T> T change(String name, Function<T, T> modifier, T defaultValue) {
        T settingVal = getSetting(name, defaultValue);
        // apply the modifier
        T applied = modifier.apply(settingVal);

        // update the setting
        setSetting(name, applied);

        return applied;
    }

    public <T> T getSetting(String name, T defaultSave) {
        Object returnValue = manager.getConfig().get(getPath(name));
        if (returnValue == null) {
            // if the file in the config is non-existent then return + set the default
            setSetting(name, defaultSave);
            return defaultSave;
        }

        // return the retrieved value
        return (T) returnValue;
    }

    public void setSetting(String name, Object value) {
        manager.getConfig().set(getPath(name), value);
    }

    public String getPath(String name) {
        return String.format("%s.%s", playerId.toString(), name);
    }
}
