package dev.luaq.kit.api.scoreboard;

import dev.luaq.kit.api.settings.PlayerSettings;
import lombok.Getter;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ScoreboardAPI {
    @Getter private static final List<PlayerScoreboard> scoreboards = new ArrayList<>();

    public static PlayerScoreboard scoreboardFor(Player player) {
        PlayerScoreboard scoreboard;
        // if the player has no realScoreboard, make one
        if (!playerHasScoreboard(player)) {
            scoreboard = new PlayerScoreboard(player);
            scoreboards.add(scoreboard); // add to the static list to prevent duplicates
        }
        else {
            // loop through using lambdas and find the proper one
            scoreboard = scoreboards.stream().filter(sb -> sb.getPlayerUUID().equals(player.getUniqueId())).findFirst().orElse(null);
        }
        return scoreboard;
    }

    public static void removeFromScoreboards(Player player) {
        Iterator<PlayerScoreboard> iterator = scoreboards.iterator();
        while (iterator.hasNext()) {
            PlayerScoreboard scoreboard = iterator.next();
            if (!scoreboard.getPlayerUUID().equals(player.getUniqueId())) {
                continue;
            }

            // remove the player from the cache
            iterator.remove();
            break;
        }
    }

    public static boolean playerHasScoreboard(Player player) {
        for (PlayerScoreboard scoreboard : scoreboards) {
            // match the player's UUID
            if (!scoreboard.getPlayerUUID().equals(player.getUniqueId())) {
                continue;
            }
            // the player's UUID matches this realScoreboard
            return true; // they do have a realScoreboard
        }
        return false; // they do not, since the loop exited
    }

    public static ChatColor getColorByLine(int line) {
        if (line < 0 || line > ChatColor.values().length) {
            return null;
        }
        // return the chat color based on the line number (max 22)
        return ChatColor.values()[line];
    }

    public static String getTeamNameByLine(int line) {
        return "line" + line;
    }
}
