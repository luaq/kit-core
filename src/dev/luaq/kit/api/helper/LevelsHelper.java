package dev.luaq.kit.api.helper;

import dev.luaq.kit.KitCore;
import dev.luaq.kit.api.settings.PlayerSettings;
import dev.luaq.kit.api.settings.SettingsManager;
import org.bukkit.entity.Player;

import java.util.AbstractMap;
import java.util.Map;
import java.util.Random;

import static dev.luaq.kit.utils.LibrixUtils.get;

public class LevelsHelper {
    public static int howMuchXp(int killCount) {
        boolean isRandom = get("leveling.xp.random");
        int random = new Random().nextInt() * (int) get("leveling.xp.randomness");

        // xp = 5 * (opponentKills / 100)
        int xp = (int) ((int) get("leveling.xp.amount") * (float) (Math.max(killCount, get("leveling.xp.killDepend")) / (int) get("leveling.xp.killDepend")));

        return xp + (isRandom ? random : 0);
    }

    public static Map.Entry<Boolean, Integer> giveXp(Player player, int amount) {
        SettingsManager manager = KitCore.instance.getSettings();
        PlayerSettings settings = manager.settingsFor(player);

        int currentXp = settings.change("levels.xp", xp -> xp + amount, 0);
        int needed = xpNeeded(settings.getSetting("levels.level", 1));
        if (currentXp >= needed) {
            // increment the level and reset the xp to overflow difference
            settings.change("levels.level", lvl -> ++lvl, 1);
            settings.setSetting("levels.xp", currentXp - needed);

            return new AbstractMap.SimpleEntry<>(true, currentXp); // level up
        }

        return new AbstractMap.SimpleEntry<>(false, currentXp);
    }

    public static float percentToNextLevel(int currentLevel, int currentXp) {
        return (float) currentXp / (float) xpNeeded(currentLevel);
    }

    public static int xpNeeded(int currentLevel) {
        return (int) get("leveling.xpPerLevel") * ((int) get("leveling.multiplierPerLevel") * currentLevel);
    }
}
