package dev.luaq.kit.api;

import com.gmail.filoghost.holographicdisplays.api.placeholder.PlaceholderReplacer;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import dev.luaq.kit.utils.LibrixUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class PlayingPlaceholder implements PlaceholderReplacer {
    @Override
    public String update() {
        List<String> pvpRegions = LibrixUtils.get("worldguard.pvpregions");
        int count = 0;

        // ignore if there are no set regions
        if (pvpRegions.isEmpty()) {
            return "0";
        }

        // go through each player and count for each in a defined pvp region
        for (Player player : Bukkit.getOnlinePlayers()) {
            Location location = player.getLocation();
            RegionManager manager = WorldGuardPlugin.inst().getRegionManager(location.getWorld());
            Set<Map.Entry<String, ProtectedRegion>> regions = manager.getRegions().entrySet();
            for (Map.Entry<String, ProtectedRegion> entry : regions) {
                String name = entry.getKey();
                ProtectedRegion region = entry.getValue();

                // check if player is in the region
                if (!region.contains(location.getBlockX(), location.getBlockY(), location.getBlockZ()) || !pvpRegions.contains(name)) {
                    continue; // they are not
                }

                // increment the count
                count++;
                break; // don't count twice
            }
        }

        return String.format("%,d", count);
    }
}
