package dev.luaq.kit;

import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;
import dev.luaq.kit.api.PlayingPlaceholder;
import dev.luaq.kit.api.settings.SettingsManager;
import dev.luaq.kit.command.GotoSpawn;
import dev.luaq.kit.command.ReloadConfig;
import dev.luaq.kit.command.SetSpawn;
import dev.luaq.kit.command.TimeChanger;
import dev.luaq.kit.handlers.ChatHandler;
import dev.luaq.kit.handlers.RespawnHandler;
import dev.luaq.kit.handlers.CombatHandler;
import dev.luaq.kit.handlers.combatlog.LoggerEvents;
import dev.luaq.kit.handlers.ConnectionHandler;
import dev.luaq.kit.handlers.combatlog.CombatLogger;
import dev.luaq.kit.handlers.tasks.BroadcastTask;
import dev.luaq.kit.handlers.tasks.LeaderboardTask;
import dev.luaq.kit.handlers.tasks.RepeatingTask;
import dev.luaq.kit.utils.LibrixUtils;
import lombok.Getter;
import net.milkbowl.vault.chat.Chat;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitScheduler;

public class KitCore extends JavaPlugin {
    public static KitCore instance;

    @Getter private SettingsManager settings;
    @Getter private Chat vaultChat;
    @Getter private boolean holograms;
    @Getter private CombatLogger combatLogger;

    @Override
    public void onEnable() {
        // save instances that will be used throughout all code
        instance = this;
        settings = new SettingsManager("players.yml");
        combatLogger = new CombatLogger();

        // check for holographic displays
        if (!(holograms = Bukkit.getPluginManager().isPluginEnabled("HolographicDisplays"))) {
            getLogger().warning("The leaderboard cannot be done without HolographicDisplays.");
        } else if (Bukkit.getPluginManager().isPluginEnabled("WorldGuard")) {
            // register the kitpvp placeholder
            HologramsAPI.registerPlaceholder(this, "kitcore_playing", 5, new PlayingPlaceholder());
        }

        vaultHook();

        // register the config
        regConfig();

        // register listeners and commands
        regHandlers();
        regCommands();

        // load the settings for the server
        settings.loadSettings();
    }

    @Override
    public void onDisable() {
        // save the file before the server goes down
        settings.saveSettings();
    }

    private void vaultHook() {
        Chat provider = getServer().getServicesManager().load(Chat.class);
        if (provider == null) {
            return;
        }

        vaultChat = provider;
    }

    private void regConfig() {
        getConfig().options().copyDefaults(true);
        saveConfig();
    }

    private void regCommands() {
        getCommand("reloadconfig").setExecutor(new ReloadConfig());

        getCommand("day").setExecutor(new TimeChanger());

        getCommand("spawn").setExecutor(new GotoSpawn());
        getCommand("setspawn").setExecutor(new SetSpawn());
    }

    private void regHandlers() {
        PluginManager man = Bukkit.getPluginManager();
        BukkitScheduler scheduler = Bukkit.getScheduler();

        man.registerEvents(new LoggerEvents(), this);
        man.registerEvents(new ConnectionHandler(), this);
        man.registerEvents(new ChatHandler(), this);
        man.registerEvents(new RespawnHandler(), this);
        man.registerEvents(new CombatHandler(), this);

        // only register the leaderboard task if holograms are on
        if (holograms) {
            // update every minute
            scheduler.scheduleSyncRepeatingTask(this, new LeaderboardTask(), 20L, 20L * 60L);
        }

        scheduler.scheduleSyncRepeatingTask(this, new RepeatingTask(), 20L, 20L);

        // register autobroadcast with an interval
        scheduler.scheduleSyncRepeatingTask(this, new BroadcastTask(), 0L, LibrixUtils.<Integer>get("autobroadcast.interval") * 20L);
    }
}
