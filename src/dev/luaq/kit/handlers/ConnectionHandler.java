package dev.luaq.kit.handlers;

import dev.luaq.kit.KitCore;
import dev.luaq.kit.api.scoreboard.ScoreboardAPI;
import dev.luaq.kit.api.settings.SettingsManager;
import dev.luaq.kit.utils.ChatUtils;
import dev.luaq.kit.utils.LibrixUtils;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class ConnectionHandler implements Listener {
    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();

        // change the join message
        event.setJoinMessage(ChatUtils.formatFL(player, "join.broadcast"));
        // send per player message
        player.sendMessage(ChatUtils.formatFL(player, "join.perPlayer"));

        // send the player to spawn
        LibrixUtils.run(() -> player.teleport(LibrixUtils.getLocation("spawn")));
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        // fuck that
        event.setQuitMessage("");

        Player player = event.getPlayer();

        // save the kit core settings
        SettingsManager manager = KitCore.instance.getSettings();
        manager.saveSettings();

        // remove the player from the cache (prevent memory leaks)
        ScoreboardAPI.removeFromScoreboards(player);
        manager.removeFromCache(player);
    }
}
