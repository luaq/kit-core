package dev.luaq.kit.handlers.tasks;

import com.gmail.filoghost.holographicdisplays.api.Hologram;
import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;
import dev.luaq.kit.KitCore;
import dev.luaq.kit.api.settings.PlayerSettings;
import dev.luaq.kit.utils.ChatUtils;
import dev.luaq.kit.utils.LibrixUtils;
import lombok.Getter;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class LeaderboardTask implements Runnable {
    @Getter private Hologram leaderboard = null;

    @Override
    public void run() {
        KitCore core = KitCore.instance;
        if (leaderboard == null) {
            // make the leaderboard
            leaderboard = HologramsAPI.createHologram(core, LibrixUtils.getLocation("lbLoc"));
        }

        List<PlayerSettings> allStats = core.getSettings().allPlayerSettings();
        allStats.sort(Comparator.comparingInt(settings -> settings.getSetting("kills", 0)));
        Collections.reverse(allStats);

        // get rid of all lines
        leaderboard.clearLines();

        // go through the first 10
        for (int i = 0; i < 10; i++) {
            PlayerSettings settings;
            try {
                // try to get
                settings = allStats.get(i);
            } catch (IndexOutOfBoundsException ignored) {
                settings = null; // make it null
            }
            String displayName = settings == null ? ChatUtils.color("&7&ovacant") : settings.getSetting("name", "none");
            String kills = settings == null ? "???" : String.format("%,d", settings.getSetting("kills", 0));

            leaderboard.appendTextLine(ChatUtils.colorL("leaderboardLine", i + 1, displayName, kills));
        }
    }
}
