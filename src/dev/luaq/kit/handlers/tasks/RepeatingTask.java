package dev.luaq.kit.handlers.tasks;

import dev.luaq.kit.KitCore;
import dev.luaq.kit.api.scoreboard.PlayerScoreboard;
import dev.luaq.kit.api.scoreboard.ScoreboardAPI;
import dev.luaq.kit.api.settings.PlayerSettings;
import dev.luaq.kit.utils.lang.LangUtils;
import org.bukkit.Bukkit;
import org.bukkit.World;

import java.util.List;
import java.util.stream.Collectors;

public class RepeatingTask implements Runnable {
    @Override
    public void run() {
        removeWeather();
        showScoreboards();
    }

    private void showScoreboards() {
        Bukkit.getOnlinePlayers().forEach(player -> {
            PlayerScoreboard scoreboard = ScoreboardAPI.scoreboardFor(player);
            PlayerSettings playerSettings = KitCore.instance.getSettings().settingsFor(player);
            // skip disabled scoreboard players
            if (!playerSettings.getSetting("scoreboard_enabled", true)) {
                scoreboard.hide();
                return;
            }

            List<String> lines = LangUtils.getLang("scoreboard.lines");

            scoreboard.setDisplayName(LangUtils.getLang("scoreboard.title"));
            // set the lines with formatted values
            scoreboard.setLines(lines.stream()
                    .map(line -> LangUtils.FORMAT.format(line, player))
                    .collect(Collectors.toList()));

            // show the scoreboard
            scoreboard.show();
        });
    }

    private void removeWeather() {
        // fuck minecraft weather
        for (World world : Bukkit.getWorlds()) {
            world.setWeatherDuration(0);
            world.setThundering(false);
            world.setStorm(false);
        }
    }
}
