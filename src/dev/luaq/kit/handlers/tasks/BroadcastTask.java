package dev.luaq.kit.handlers.tasks;

import dev.luaq.kit.KitCore;
import dev.luaq.kit.api.settings.PlayerSettings;
import dev.luaq.kit.utils.ChatUtils;
import dev.luaq.kit.utils.LibrixUtils;
import dev.luaq.kit.utils.lang.LangUtils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.List;

public class BroadcastTask implements Runnable {
    private final List<String> messages;
    private int i = 0;

    public BroadcastTask() {
        this.messages = LibrixUtils.get("autobroadcast.broadcasts");
    }

    @Override
    public void run() {
        if (!LibrixUtils.<Boolean>get("autobroadcast.enabled")) {
            return;
        }

        // construct the broadcast message
        StringBuilder builder = new StringBuilder(LibrixUtils.get("autobroadcast.prefix"));
        builder.append(messages.get(i)).append(LibrixUtils.<String>get("autobroadcast.suffix"));

        for (Player player : Bukkit.getOnlinePlayers()) {
            PlayerSettings manager = KitCore.instance.getSettings().settingsFor(player);
            // if they have broadcasts disabled then skip sending it to them
            if (!manager.getSetting("broadcast_enabled", true)) {
                continue;
            }

            // send the colored message with placeholders handled
            player.sendMessage(ChatUtils.color(LangUtils.FORMAT.format(builder.toString(), player)));
        }

        // loop i if it's the size of
        if (++i >= messages.size()) {
            i = 0;
        }
    }
}
