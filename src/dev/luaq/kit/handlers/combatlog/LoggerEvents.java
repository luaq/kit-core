package dev.luaq.kit.handlers.combatlog;

import dev.luaq.kit.KitCore;
import dev.luaq.kit.utils.ChatUtils;
import dev.luaq.kit.utils.LibrixUtils;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LoggerEvents implements Listener {
    @EventHandler(ignoreCancelled = true)
    public void onStab(EntityDamageByEntityEvent event) {
        if (!LibrixUtils.<Boolean>get("combatlogger.enabled")) {
            return;
        }

        // if neither the attacker or attacked is a player then who cares
        if (!(event.getDamager() instanceof Player) || !(event.getEntity() instanceof Player)) {
            return;
        }

        Player attacker = (Player) event.getDamager();
        Player damaged = (Player) event.getEntity();

        CombatLogger combatLogger = KitCore.instance.getCombatLogger();

        // tell both players they've engaged (if they're not already in combat)
        if (!combatLogger.inCombat(attacker)) attacker.sendMessage(ChatUtils.colorL("combatlogger.engaged", damaged.getName()));
        if (!combatLogger.inCombat(damaged)) damaged.sendMessage(ChatUtils.colorL("combatlogger.engaged", attacker.getName()));

        // engage both players
        combatLogger.engaged(attacker, damaged);
    }

    @EventHandler
    public void onCommand(PlayerCommandPreprocessEvent event) {
        Player player = event.getPlayer();
        CombatLogger logger = KitCore.instance.getCombatLogger();

        if (!logger.inCombat(player)) {
            return; // not in combat
        }

        // remove the slash from the command
        String command = event.getMessage().substring(1);
        String[] words = command.split(" ");

        if (words.length == 0) {
            return; // ????
        }

        // check for the command
        if (LibrixUtils.<List<String>>get("combatlogger.disabledCommands").contains(words[0])) {
            // let them know and cancel the command
            player.sendMessage(ChatUtils.colorL("combatlogger.inCombat", logger.timeLeft(player)));
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onDeath(PlayerDeathEvent event) {
        // disable player that died combat logger
        KitCore.instance.getCombatLogger().disengage(event.getEntity());
    }

    @EventHandler
    public void onLogout(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        Location location = player.getLocation();

        if (!KitCore.instance.getCombatLogger().inCombat(player)) {
            return; // if they're not in combat then don't do anything
        }

        // strike lightning where they die
        location.getWorld().strikeLightningEffect(location);
        player.setHealth(0); // kill the player after it all
    }
}
