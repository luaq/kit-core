package dev.luaq.kit.handlers.combatlog;

import dev.luaq.kit.utils.LibrixUtils;
import dev.luaq.kit.utils.lang.LangUtils;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class CombatLogger {
    private final Map<UUID, Long> playersInCombat = new HashMap<>();

    public String timeLeft(Player player) {
        if (!inCombat(player)) {
            return LangUtils.getLang("combatlogger.safe");
        }

        long currentTime = System.currentTimeMillis() / 1000;
        long regTime = playersInCombat.get(player.getUniqueId()) / 1000;

        return String.format("%ds", LibrixUtils.<Integer>get("combatlogger.cooldown") - (currentTime - regTime));
    }

    public void engaged(Player... players) {
        long time = System.currentTimeMillis();
        // for every player that engaged in combat
        // set their engagement time
        for (Player player : players) {
            playersInCombat.put(player.getUniqueId(), time);
        }
    }

    public void disengage(Player player) {
        playersInCombat.remove(player.getUniqueId());
    }

    public boolean inCombat(Player player) {
        if (player.isOp() && LibrixUtils.<Boolean>get("combatlogger.opsBypass")) {
            return false;
        }

        UUID uuid = player.getUniqueId();
        // the cooldown in milliseconds
        long cooldown = LibrixUtils.<Integer>get("combatlogger.cooldown") * 1000L;

        if (System.currentTimeMillis() - playersInCombat.getOrDefault(uuid, 0L) > cooldown) {
            // remove the player from the list if they are no longer on cooldown
            playersInCombat.remove(uuid);
            return false;
        }

        return true;
    }
}
