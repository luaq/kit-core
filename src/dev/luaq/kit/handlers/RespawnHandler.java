package dev.luaq.kit.handlers;

import dev.luaq.kit.utils.LibrixUtils;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RespawnHandler implements Listener {
    @EventHandler
    public void onDeath(PlayerDeathEvent event) {
        Player player = event.getEntity();
        Location location = player.getLocation();
        PlayerInventory inventory = player.getInventory();

        // drop every item in the player's inventory where they died
        List<ItemStack> items = new ArrayList<>();

        // combine the items
        items.addAll(Arrays.asList(inventory.getContents()));
        items.addAll(Arrays.asList(inventory.getArmorContents()));

        for (ItemStack itemStack : items) {
            if (itemStack == null || itemStack.getType() == Material.AIR) {
                continue;
            }
            location.getWorld().dropItemNaturally(location, itemStack);
        }
        // clear the contents of their inventory
        inventory.clear();

        player.setHealth(20.0);
        // set the player's velocity to 0
        player.setVelocity(player.getVelocity().zero());
        player.teleport(LibrixUtils.getLocation("spawn"));
    }
}
