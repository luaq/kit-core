package dev.luaq.kit.handlers;

import dev.luaq.kit.KitCore;
import dev.luaq.kit.api.helper.LevelsHelper;
import dev.luaq.kit.api.settings.PlayerSettings;
import dev.luaq.kit.api.settings.SettingsManager;
import dev.luaq.kit.utils.ChatUtils;
import dev.luaq.kit.utils.LibrixUtils;
import dev.luaq.kit.utils.lang.LangUtils;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

import java.util.Map;

public class CombatHandler implements Listener {
    @EventHandler
    public void onKill(PlayerDeathEvent event) {
        KitCore core = KitCore.instance;
        SettingsManager settings = core.getSettings();

        Player died = event.getEntity();
        PlayerSettings diedStats = settings.settingsFor(died);

        Player killer = died.getKiller(); // the mad lad that won
        PlayerSettings killerStats = settings.settingsFor(killer);

        // set the death message to nothing incase
        event.setDeathMessage("");

        if (killer == null) { // this is an obvious check, where was it?
            return;
        }

        // send custom kill message
        String killPack = killerStats.getSetting("custom.killMessages", "default");
        String damageCause = died.getLastDamageCause().getCause().toString().toLowerCase();
        String killMessage = LangUtils.getLang(String.format("deathMessages.%s.messages.%s", killPack, damageCause));

        if (killMessage != null) {
            // set the custom colored message
            event.setDeathMessage(ChatUtils.color(String.format(killMessage, died.getName(), killer.getName())));
        }

        // update each stat
        diedStats.change("deaths", d -> ++d, 0);
        killerStats.change("kills", k -> ++k, 0);

        // the opponent's kill count
        int opKills = diedStats.getSetting("kills", 0);
        int xpToGive = LevelsHelper.howMuchXp(opKills);

        int currentLevel = killerStats.getSetting("levels.level", 1);

        // whether level up is true and the current xp
        Map.Entry<Boolean, Integer> giveResponse = LevelsHelper.giveXp(killer, xpToGive);
        boolean levelUp = giveResponse.getKey();
        int xp = giveResponse.getValue();

        if (levelUp) {
            killer.sendMessage(ChatUtils.colorL("leveling.levelup", currentLevel + 1, "DISABLED"));
            // do something special
            return;
        }

        killer.sendMessage(ChatUtils.colorL("leveling.gainXp", xpToGive, "killing " + died.getName(), LevelsHelper.percentToNextLevel(currentLevel, xp) * 100));
    }
}
