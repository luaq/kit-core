package dev.luaq.kit.handlers;

import dev.luaq.kit.KitCore;
import dev.luaq.kit.api.settings.PlayerSettings;
import dev.luaq.kit.utils.ChatUtils;
import dev.luaq.kit.utils.LibrixUtils;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class ChatHandler implements Listener {
    @EventHandler(ignoreCancelled = true)
    public void onChat(AsyncPlayerChatEvent event) {
        // cancel the actual message
        event.setCancelled(true);

        Player player = event.getPlayer();
        KitCore core = KitCore.instance;
        PlayerSettings settings = core.getSettings().settingsFor(player);

        // check if they have their chat disabled
        if (settings.getSetting("chat_disabled", false)) {
            player.sendMessage(ChatUtils.colorL("chat.disabled"));
            return;
        }

        String toSend = event.getMessage();
        if (player.hasPermission(LibrixUtils.<String>get("chat.colorPerm"))) {
            toSend = ChatUtils.color(toSend);
        }

        boolean showMessage = toSend.endsWith(LibrixUtils.get("chat.bypassChar")) && player.hasPermission(LibrixUtils.<String>get("chat.bypassPerm"));

        String format = ChatUtils.color(ChatUtils.formatFL(player, "chat.format"));
        String messageToSend = String.format(format, toSend);

        // send message to all players that have chat enabled
        LibrixUtils.messageAll(messageToSend, recipient -> {
            PlayerSettings recipSettings = core.getSettings().settingsFor(recipient);
            return !recipSettings.getSetting("chat.disabled", false) || showMessage;
        }, false);
    }
}
