package dev.luaq.kit.command;

import dev.luaq.kit.utils.ChatUtils;
import dev.luaq.kit.utils.LibrixUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class GotoSpawn implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (!(sender instanceof Player)) {
            return false;
        }

        Player player = (Player) sender;
        if (args.length > 0 && player.hasPermission("kitcore.spawnothers")) {
            Player target = Bukkit.getPlayer(args[0]);
            // if the target is null then no
            if (target == null) {
                player.sendMessage(ChatUtils.colorL("playerNotFound"));
                return true;
            }

            // set the player to target
            player = target;
        }

        // get the location and teleport them to it
        player.teleport(LibrixUtils.getLocation("spawn"));
        player.sendMessage(ChatUtils.colorL("warpedTo", "spawn"));

        return true;
    }
}
