package dev.luaq.kit.command;

import dev.luaq.kit.KitCore;
import dev.luaq.kit.utils.ChatUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class ReloadConfig implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        KitCore.instance.reloadConfig();
        commandSender.sendMessage(ChatUtils.colorL("reloadedConfig"));

        return true;
    }
}
