package dev.luaq.kit.command;

import dev.luaq.kit.utils.ChatUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

public class TimeChanger implements CommandExecutor {
    private final Map<String, Long> times = new HashMap<>();

    public TimeChanger() {
        times.put("day", 1000L);
        times.put("sunset", 13000L);
        times.put("night", 18000L);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] strings) {
        if (!(sender instanceof Player)) {
            return false;
        }

        Player player = (Player) sender;
        player.setPlayerTime(times.getOrDefault(s, 0L), false);
        sender.sendMessage(ChatUtils.colorL("timeChanged", s));

        return true;
    }
}
