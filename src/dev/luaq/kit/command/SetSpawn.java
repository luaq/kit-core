package dev.luaq.kit.command;

import dev.luaq.kit.utils.ChatUtils;
import dev.luaq.kit.utils.LibrixUtils;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetSpawn implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] strings) {
        if (!(sender instanceof Player)) {
            return false; // no console >:(
        }

        // store the player location in the config
        Player player = (Player) sender;
        Location location = player.getLocation();

        // set all values of the location
        LibrixUtils.set("spawn.world", location.getWorld().getName());
        LibrixUtils.set("spawn.x", location.getX());
        LibrixUtils.set("spawn.y", location.getY());
        LibrixUtils.set("spawn.z", location.getZ());
        LibrixUtils.set("spawn.yaw", String.valueOf(location.getYaw()));
        LibrixUtils.set("spawn.pitch", String.valueOf(location.getPitch()));

        player.sendMessage(ChatUtils.colorL("setSpawn"));

        return true;
    }
}
