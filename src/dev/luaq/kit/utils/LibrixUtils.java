package dev.luaq.kit.utils;

import dev.luaq.kit.KitCore;
import dev.luaq.kit.utils.lang.LangUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

import java.util.function.Predicate;

public class LibrixUtils {
    public static void set(String configPath, Object object) {
        KitCore kitCore = KitCore.instance;

        kitCore.getConfig().set(configPath, object);
        // save any changes
        kitCore.saveConfig();
    }

    public static <T> T get(String configPath) {
        // could be improved but fuck you <3
        return (T) KitCore.instance.getConfig().get(configPath);
    }

    public static Location getLocation(String configPath) {
        World world = Bukkit.getWorld(LibrixUtils.<String>get(String.format("%s.world", configPath)));
        if (world == null) {
            return null;
        }

        // very awful way of doing things, not recommended (fuck yaw and pitch btw)
        return new Location(world, get(configPath + ".x"), get(configPath + ".y"), get(configPath + ".z"), Float.parseFloat(LibrixUtils.get(configPath + ".yaw")), Float.parseFloat(get(configPath + ".pitch")));
    }

    public static void messageAll(String message) {
        // default to all allowed
        messageAll(message, ignored -> true);
    }

    public static void messageAll(String message, Predicate<Player> filter) {
        messageAll(message, filter, true);
    }

    public static void messageAll(String message, Predicate<Player> filter, boolean color) {
        // send a message to all players that match a proper filter
        Bukkit.getOnlinePlayers().stream().filter(filter)
                .forEach(player -> player.sendMessage(color ? ChatUtils.formatFL(player, message) : message));
    }

    public static void messageAllL(String langPath) {
        messageAllL(langPath, ignored -> true);
    }

    public static void messageAllL(String langPath, Predicate<Player> filter) {
        // get the language string and send it all
        String lang = LangUtils.getLang(langPath);
        messageAll(lang, filter);
    }

    public static void run(Runnable run) {
        Bukkit.getScheduler().runTaskLater(KitCore.instance, run, 1L);
    }
}
