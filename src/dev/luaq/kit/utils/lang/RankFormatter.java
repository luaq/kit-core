package dev.luaq.kit.utils.lang;

import dev.luaq.kit.KitCore;
import dev.luaq.kit.utils.lang.format.IFormatter;
import net.milkbowl.vault.chat.Chat;
import org.bukkit.entity.Player;

public class RankFormatter implements IFormatter {
    @Override
    public String onRequest(String nameId, Player player) {
        Chat chat = KitCore.instance.getVaultChat();
        if (chat == null || player == null) {
            return null;
        }

        if (nameId.equalsIgnoreCase("rank_prefix")) {
            return chat.getPlayerPrefix(player);
        }

        if (nameId.equalsIgnoreCase("rank_suffix")) {
            return chat.getPlayerSuffix(player);
        }

        return null;
    }
}
