package dev.luaq.kit.utils.lang.format;

import org.bukkit.entity.Player;

public interface IFormatter {
    String onRequest(String nameId, Player player);
}
