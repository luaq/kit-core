package dev.luaq.kit.utils.lang.format;

import lombok.Getter;
import org.bukkit.entity.Player;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FormatHelper {
    @Getter private final List<IFormatter> formatters = new ArrayList<>();
    private final static Pattern VARIABLE_PATTERN = Pattern.compile("%(?<name>[A-z0-9-_]+)%");

    public String format(String input, @Nullable Player player) {
        // go through each formatter and find keywords
        for (IFormatter formatter : formatters) {
            Matcher matcher = VARIABLE_PATTERN.matcher(input);
            // find all matches
            while (matcher.find()) {
                String name = matcher.group("name");
                if (name == null) {
                    continue;
                }
                // get the value of name id
                String value = formatter.onRequest(name, player);
                // don't replace values that don't exist
                if (value == null) {
                    continue;
                }
                // give the new value
                input = input.replace(String.format("%%%s%%", name), value);
            }
        }
        return input;
    }
}
