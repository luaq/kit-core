package dev.luaq.kit.utils.lang;

import dev.luaq.kit.utils.LibrixUtils;
import dev.luaq.kit.utils.lang.format.FormatHelper;

public class LangUtils {
    private static final String LANG_PREFIX = "lang";
    public static final FormatHelper FORMAT = new FormatHelper();

    static {
        FORMAT.getFormatters().add(new DefaultFormatter());
        FORMAT.getFormatters().add(new RankFormatter());
    }

    public static <T> T getLang(String name) {
        return LibrixUtils.get(String.format("%s.%s", LANG_PREFIX, name));
    }
}
