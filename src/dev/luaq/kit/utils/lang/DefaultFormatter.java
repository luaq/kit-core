package dev.luaq.kit.utils.lang;

import dev.luaq.kit.KitCore;
import dev.luaq.kit.api.helper.LevelsHelper;
import dev.luaq.kit.api.settings.PlayerSettings;
import dev.luaq.kit.handlers.combatlog.CombatLogger;
import dev.luaq.kit.utils.LibrixUtils;
import dev.luaq.kit.utils.lang.format.IFormatter;
import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.entity.Player;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class DefaultFormatter implements IFormatter {
    @Override
    public String onRequest(String nameId, Player player) {
        // save this for later use
        Server server = Bukkit.getServer();

        // return the members online
        if (nameId.equalsIgnoreCase("server_on")) {
            // parse the integer as a string
            return String.valueOf(server.getOnlinePlayers().size());
        }

        if (nameId.equalsIgnoreCase("server_max")) {
            // return the parsed maximum of players
            return String.valueOf(server.getMaxPlayers());
        }

        // the following don't get replaced if no player
        // don't care if the player is null for the following
        if (player == null) {
            return null;
        }

        if (nameId.equalsIgnoreCase("player_name")) {
            // returns the name of the player passed
            return player.getName();
        }

        // the player stats
        KitCore core = KitCore.instance;
        PlayerSettings settings = core.getSettings().settingsFor(player);
        NumberFormat decimalF = new DecimalFormat("#,###.##");

        // keep kills and deaths saved
        int kills = settings.getSetting("kills", 0);
        int deaths = settings.getSetting("deaths", 0);

        int level = settings.getSetting("levels.level", 1);
        int xp = settings.getSetting("levels.xp", 0);
        float toNextLevel = LevelsHelper.percentToNextLevel(level, xp);

        if (nameId.equalsIgnoreCase("player_level")) {
            // return the player's level
            return String.format("%,d", settings.getSetting("levels.level", 1));
        }

        if (nameId.equalsIgnoreCase("player_lvlp_a")) {
            return StringUtils.repeat("=", (int) (toNextLevel * 6));
        }

        if (nameId.equalsIgnoreCase("player_lvlp_b")) {
            return StringUtils.repeat("-", 6 - (int) (toNextLevel * 6));
        }

        if (nameId.equalsIgnoreCase("player_lvlp")) {
            // return the percentage until the next level
            return String.format("%.1f", toNextLevel * 100);
        }

        if (nameId.equalsIgnoreCase("player_kills")) {
            // return the kill count and default to 0
            return String.format("%,d", kills);
        }

        if (nameId.equalsIgnoreCase("player_deaths")) {
            // return the default count and default to 0
            return String.format("%,d", deaths);
        }

        if (nameId.equalsIgnoreCase("player_kdr")) {
            // give the kill death ratio but use 1 as a default for deaths
            return decimalF.format(((double) kills) / (((double) deaths) == 0 ? 1 : ((double) deaths)));
        }

        if (nameId.equalsIgnoreCase("player_combat_status")) {
            // return the time left string
            CombatLogger logger = core.getCombatLogger();
            return logger.timeLeft(player);
        }

        if (nameId.equalsIgnoreCase("player_bal")) {
            // get the player's balance and set it to the default
            double defaultBal = LibrixUtils.<Double>get("defaultBalance");
            return decimalF.format(settings.getSetting("coinsBalance", defaultBal));
        }

        return null;
    }
}
