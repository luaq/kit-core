package dev.luaq.kit.utils;

import dev.luaq.kit.utils.lang.LangUtils;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class ChatUtils {
    public static String color(String s) {
        return ChatColor.translateAlternateColorCodes('&', s);
    }

    public static String formatFL(Player player, String path) {
        return LangUtils.FORMAT.format(color(LangUtils.getLang(path)), player);
    }

    public static String colorL(String path, Object... formats) {
        return String.format(color(LangUtils.getLang(path)), formats);
    }
}
